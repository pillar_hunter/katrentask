﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TestTask
{
    public class Program
    {
        private static string Vovels = "aeiouAEIOUаеёиоуыэюяАЕЁИОУЫЭЮЯ";

        /// <summary>
        /// Программа принимает на входе 2 пути до файлов.
        /// Анализирует в первом файле кол-во вхождений каждой буквы (регистрозависимо). Например А, б, Б, Г и т.д.
        /// Анализирует во втором файле кол-во вхождений парных букв (не регистрозависимо). Например АА, Оо, еЕ, тт и т.д.
        /// По окончанию работы - выводит данную статистику на экран.
        /// </summary>
        /// <param name="args">Первый параметр - путь до первого файла.
        /// Второй параметр - путь до второго файла.</param>
        static void Main(string[] args)
        {
            IReadOnlyStream inputStream1 = GetInputStream(args[0]);
            IReadOnlyStream inputStream2 = GetInputStream(args[1]);

            List<LetterStats> singleLetterStats = FillSingleLetterStats(inputStream1);
            List<LetterStats> doubleLetterStats = FillDoubleLetterStats(inputStream2);

            inputStream1.Dispose();
            inputStream2.Dispose();

            PrintStatistic(RemoveCharStatsByType(singleLetterStats, CharType.Vowel));
            PrintStatistic(RemoveCharStatsByType(doubleLetterStats, CharType.Consonants));

            Console.ReadKey();
        }

        /// <summary>
        /// Ф-ция возвращает экземпляр потока с уже загруженным файлом для последующего посимвольного чтения.
        /// </summary>
        /// <param name="fileFullPath">Полный путь до файла для чтения</param>
        /// <returns>Поток для последующего чтения.</returns>
        private static IReadOnlyStream GetInputStream(string fileFullPath)
        {
            return new ReadOnlyStream(fileFullPath);
        }

        /// <summary>
        /// Ф-ция считывающая из входящего потока все буквы, и возвращающая коллекцию статистик вхождения каждой буквы.
        /// Статистика РЕГИСТРОЗАВИСИМАЯ!
        /// </summary>
        /// <param name="stream">Стрим для считывания символов для последующего анализа</param>
        /// <returns>Коллекция статистик по каждой букве, что была прочитана из стрима.</returns>
        private static List<LetterStats> FillSingleLetterStats(IReadOnlyStream stream)
        {
            stream.ResetPositionToStart();
            List<LetterStats> stats = new List<LetterStats>();
            while (!stream.IsEof)
            {
                char ch = stream.ReadNextChar();
                if (char.IsLetter(ch))
                {
                    IncStatistic(stats, ch.ToString());
                }
            }
            return stats;
        }

        /// <summary>
        /// Ф-ция считывающая из входящего потока все буквы, и возвращающая коллекцию статистик вхождения парных букв.
        /// В статистику должны попадать только пары из одинаковых букв, например АА, СС, УУ, ЕЕ и т.д.
        /// Статистика - НЕ регистрозависимая!
        /// </summary>
        /// <param name="stream">Стрим для считывания символов для последующего анализа</param>
        /// <returns>Коллекция статистик по каждой букве, что была прочитана из стрима.</returns>
        private static List<LetterStats> FillDoubleLetterStats(IReadOnlyStream stream)
        {
            stream.ResetPositionToStart();
            List<LetterStats> stats = new List<LetterStats>();
            char[] pair = new char[2];
            pair[0] = stream.ReadNextChar();
            while (!stream.IsEof)
            {
                pair[1] = stream.ReadNextChar();
                if (char.IsLetter(pair[0]) && char.IsLetter(pair[1]))
                {
                    if (char.ToUpper(pair[0]) == char.ToUpper(pair[1]))
                    {
                        IncStatistic(stats, new string(pair).ToUpper());
                    }
                }
                pair[0] = pair[1];
            }
            return stats;
        }

        /// <summary>
        /// Ф-ция перебирает все найденные буквы/парные буквы, содержащие в себе только гласные или согласные буквы.
        /// (Тип букв для перебора определяется параметром charType)
        /// Все найденные буквы/пары соответствующие параметру поиска - удаляются из переданной коллекции статистик.
        /// </summary>
        /// <param name="letters">Коллекция со статистиками вхождения букв/пар</param>
        /// <param name="charType">Тип букв для анализа</param>
        private static List<LetterStats> RemoveCharStatsByType(List<LetterStats> letters, CharType charType)
        {
            switch (charType)
            {
                case CharType.Consonants:
                    return letters.Where(stats => stats.Letter.All(ch => !IsVovel(ch))).ToList();
                case CharType.Vowel:
                    return letters.Where(stats => stats.Letter.All(ch => IsVovel(ch))).ToList();
                default:
                    return letters;
            }
        }

        /// <summary>
        /// Ф-ция выводит на экран полученную статистику в формате "{Буква} : {Кол-во}"
        /// Каждая буква - с новой строки.
        /// Выводить на экран необходимо предварительно отсортировав набор по алфавиту.
        /// В конце отдельная строчка с ИТОГО, содержащая в себе общее кол-во найденных букв/пар
        /// </summary>
        /// <param name="letters">Коллекция со статистикой</param>
        private static void PrintStatistic(List<LetterStats> letters)
        {
            Sort(letters);
            int sum = 0;
            foreach (var l in letters)
            {
                Console.WriteLine($"{l.Letter} : {l.Count}");
                sum += l.Count;
            }
            Console.WriteLine($"Итого: {sum}");
        }

        /// <summary>
        /// Метод увеличивает счётчик вхождений по переданной структуре.
        /// </summary>
        /// <param name="letterStats"></param>
        private static void IncStatistic(List<LetterStats> letterStats, string letter)
        {   
            LetterStats ls = letterStats.Find(s => s.Letter == letter);
            if (ls != null)
            {
                ls.Count++;
            }
            else
            {
                letterStats.Add(new LetterStats(letter));
            }
        }

        private static bool IsVovel(char ch) => Vovels.Contains(ch);

        private static void Sort(List<LetterStats> stats)
        {
            stats.Sort(delegate (LetterStats x, LetterStats y)
            {
                return x.Letter.CompareTo(y.Letter);
            });
        }
    }
}